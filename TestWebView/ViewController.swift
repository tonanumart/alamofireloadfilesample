//
//  ViewController.swift
//  TestWebView
//
//  Created by Anumart on 4/24/2561 BE.
//  Copyright © 2561 CONNEX Bussiness Online. All rights reserved.
//

import UIKit
import Alamofire


class ViewController: UIViewController ,UIWebViewDelegate {
    
    
    @IBOutlet weak var webView: UIWebView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
        let url = URL (string: "http://192.168.0.98/UpDown")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    }

    @IBAction func clickBtn(_ sender: UIButton) {
    
        let url = URL (string: "http://192.168.0.98/UpDown")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        let equlsStr = "http://192.168.0.98/UpDown/api/upload/download1"
        
        if let url = request.url?.absoluteString {
            
            if(url.isEqual(equlsStr)){
                print("Handle download")
                var fPathSave:URL? = nil
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
                    let file = directoryURL.appendingPathComponent("TmpDownload", isDirectory: false)
                    print("fileSave at \(file)")
                    fPathSave = file
                    return (file, [.createIntermediateDirectories, .removePreviousFile])
                }
                //let destination = DownloadRequest.suggestedDownloadDestination(for: .documentDirectory)
                Alamofire.download(
                    url,
                    method: .get,
                    parameters: nil,
                    encoding: JSONEncoding.default,
                    headers: nil,
                    to: destination).downloadProgress(closure: { (progress) in
                        print("Download Progress:---------- \(progress.fractionCompleted)")
                    }).response(completionHandler: { (downloadResponse) in
                        if let response = downloadResponse.response ,
                            let attachNameHeader = response.allHeaderFields["Content-Disposition"]{
                            if let tmpFname = attachNameHeader as? String {
                                let fname = tmpFname.replacingOccurrences(of: "attachment; filename=", with: "")
                                print("---------------------------refilename name---------------------------")
                                print(fname)
                                //print("renaming file \(fPathSave?.lastPathComponent!) to \(fname)")
                                do {
                                    let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                                    let documentDirectory = URL(fileURLWithPath: path)
                                    let originPath = documentDirectory.appendingPathComponent("TmpDownload")
                                    let destinationPath = documentDirectory.appendingPathComponent(fname)
                                    do {try FileManager.default.removeItem(at: destinationPath)}
                                    catch {}
                                    try FileManager.default.moveItem(at: originPath, to: destinationPath)
                                    print("renaming success")
                                }catch {
                                    print(error)
                                }
                                
                            }
                        }
                    })
                return false
            }
        }
        
        return true
    }
    
    
    


}

